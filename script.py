"""
Testo:
Questo è lo spoken language dell'abstract contract 1 con riferimento alle seguenti fasce orarie: dalle 20 alle 7 20euro kw dalle 7 alle 20 30euro kw
"""

"""
input format:
15kw, 19:55
15kw, 19:55
04kw, 04:55
15kw, 19:55
15kw, 19:55
15kw, 19:55
20kw, 20:00
"""
import sys
import datetime

input_file = sys.argv[1]

file = open(input_file, "r")
buff = file.readlines()
file.close()
tot = 0
for line in buff:
    kw = int(line.strip().split(",")[0][:-2])
    orario = line.strip().split(",")[1].strip()
    ora = int(orario[:2])
    minuto = int(orario[3:])
    #print("kw", kw)
    #print("ora", ora)
    #print("minuto", minuto)
    if (ora >= 20) and (ora <= 7):
        if ora == 7:
            if minuto > 0:
                tot = tot+(kw*30)
            else:
                tot = tot+(kw*20)
    else:
        tot = tot+(kw*30)
print(tot)
