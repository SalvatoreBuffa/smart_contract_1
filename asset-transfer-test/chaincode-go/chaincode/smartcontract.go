package chaincode

import (
	"bufio"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/hyperledger/fabric-contract-api-go/contractapi"
)

// SmartContract provides functions for managing an Asset
type SmartContract struct {
	contractapi.Contract
}

type Asset struct {
	ID             string `json:"ID"`
	EnergiaErogata int    `json:"EnergiaErogata"`
	NomeAcquirente string `json:"NomeAcquirente"`
	NomeVenditore  string `json:"NomeVenditore"`
	PrezzoFascia1  int    `json:"PrezzoFascia1"`
	PrezzoFascia2  int    `json:"PrezzoFascia2"`
	Timestamp      string `json:"Timestamp"`
}

// InitBlockChain ...
func (s *SmartContract) InitBlockChain(ctx contractapi.TransactionContextInterface) error {
	current_time := time.Now()
	assets := []Asset{
		{ID: "00001", EnergiaErogata: 20, NomeAcquirente: "Mirko",
			NomeVenditore: "Mirko", PrezzoFascia1: 10, PrezzoFascia2: 20, Timestamp: current_time.Format("2006-01-02 15:04:05")},
		{ID: "00002", EnergiaErogata: 30, NomeAcquirente: "Salvatore",
			NomeVenditore: "Salvatore", PrezzoFascia1: 10, PrezzoFascia2: 20, Timestamp: current_time.Format("2006-01-02 15:04:05")},
		{ID: "00003", EnergiaErogata: 10, NomeAcquirente: "Pierluigi",
			NomeVenditore: "Mirko", PrezzoFascia1: 10, PrezzoFascia2: 20, Timestamp: current_time.Format("2006-01-02 15:04:05")},
	}
	for _, asset := range assets {
		assetJSON, err := json.Marshal(asset)
		if err != nil {
			return err
		}

		err = ctx.GetStub().PutState(asset.ID, assetJSON)
		if err != nil {
			return fmt.Errorf("failed to put to world state. %v", err)
		}
	}

	return nil
}

// InitBlockChainFromFile ..
//Esempio input file:
/*
input.txt
15kw, 19:55
15kw, 19:55
04kw, 04:55
15kw, 19:55
15kw, 19:55
15kw, 19:55
20kw, 20:00
*/
func (s *SmartContract) InitBlockChainFromFile(ctx contractapi.TransactionContextInterface) error {
	energia, orario := ReadInputFile("input.txt")
	//current_time := time.Now()
	for i := 0; i < len(energia); i++ {
		assets := []Asset{
			{ID: strconv.Itoa(i), EnergiaErogata: energia[i], NomeAcquirente: "x",
				NomeVenditore: "x", PrezzoFascia1: 30, PrezzoFascia2: 40, Timestamp: ConvertTimeToDate(orario[i])},
		}
		for _, asset := range assets {
			assetJSON, err := json.Marshal(asset)
			if err != nil {
				return err
			}

			err = ctx.GetStub().PutState(asset.ID, assetJSON)
			if err != nil {
				return fmt.Errorf("failed to put to world state. %v", err)
			}
		}
	}

	return nil
}

func (s *SmartContract) SumAllTransaction(ctx contractapi.TransactionContextInterface) int {
	assets, err := s.GetAllTransaction(ctx)
	if err != nil {
		fmt.Errorf("failed to get transaction. %v", err)
		WriteFile(0)
		return 0
	}
	sum := 0
	for _, asset := range assets {
		timeStamp, _ := time.Parse("2006-01-02 15:04:05", asset.Timestamp)
		hr, min, _ := timeStamp.Clock()
		if (hr >= 20) && (hr <= 7) {
			if hr == 7 {
				if min > 0 {
					sum += (asset.PrezzoFascia1 * asset.EnergiaErogata)
				} else {
					sum += (asset.PrezzoFascia2 * asset.EnergiaErogata)
				}
			}
		} else {
			sum += (asset.PrezzoFascia1 * asset.EnergiaErogata)
		}

	}
	WriteFile(sum)
	return sum
}

func WriteFile(value int) {
	f, err := os.Create("output.txt")
	if err != nil {
		fmt.Printf("error creating file: %v", err)
		return
	}
	defer f.Close()
	_, err = f.WriteString(fmt.Sprintf("%d\n", value))
	if err != nil {
		fmt.Printf("error writing string: %v", err)
	}
}

// CreateTransaction issues a new asset to the world state with given details.
func (s *SmartContract) CreateTransaction(ctx contractapi.TransactionContextInterface, id string, EnergiaErogata int, NomeAcquirente string, NomeVenditore string, PrezzoFascia1 int, PrezzoFascia2 int, Timestamp string) error {
	exists, err := s.TransactionExists(ctx, id)
	if err != nil {
		return err
	}
	if exists {
		return fmt.Errorf("the asset %s already exists", id)
	}

	asset := Asset{
		ID:             id,
		EnergiaErogata: EnergiaErogata,
		NomeAcquirente: NomeAcquirente,
		NomeVenditore:  NomeVenditore,
		PrezzoFascia1:  PrezzoFascia1,
		PrezzoFascia2:  PrezzoFascia2,
		Timestamp:      Timestamp,
	}
	assetJSON, err := json.Marshal(asset)
	if err != nil {
		return err
	}

	return ctx.GetStub().PutState(id, assetJSON)
}

// ReadAsset returns the asset stored in the world state with given id.
func (s *SmartContract) ReadTransaction(ctx contractapi.TransactionContextInterface, id string) (*Asset, error) {
	assetJSON, err := ctx.GetStub().GetState(id)
	if err != nil {
		return nil, fmt.Errorf("failed to read from world state: %v", err)
	}
	if assetJSON == nil {
		return nil, fmt.Errorf("the asset %s does not exist", id)
	}

	var asset Asset
	err = json.Unmarshal(assetJSON, &asset)
	if err != nil {
		return nil, err
	}

	return &asset, nil
}

// UpdateAsset updates an existing asset in the world state with provided parameters.
func (s *SmartContract) UpdatTransaction(ctx contractapi.TransactionContextInterface, id string, EnergiaErogata int, NomeAcquirente string, NomeVenditore string, PrezzoFascia1 int, PrezzoFascia2 int, Timestamp string) error {
	exists, err := s.TransactionExists(ctx, id)
	if err != nil {
		return err
	}
	if !exists {
		return fmt.Errorf("the asset %s does not exist", id)
	}

	// overwriting original asset with new asset
	asset := Asset{
		ID:             id,
		EnergiaErogata: EnergiaErogata,
		NomeAcquirente: NomeAcquirente,
		NomeVenditore:  NomeVenditore,
		PrezzoFascia1:  PrezzoFascia1,
		PrezzoFascia2:  PrezzoFascia2,
		Timestamp:      Timestamp,
	}
	assetJSON, err := json.Marshal(asset)
	if err != nil {
		return err
	}

	return ctx.GetStub().PutState(id, assetJSON)
}

// DeleteAsset deletes an given asset from the world state.
func (s *SmartContract) DeleteTransaction(ctx contractapi.TransactionContextInterface, id string) error {
	exists, err := s.TransactionExists(ctx, id)
	if err != nil {
		return err
	}
	if !exists {
		return fmt.Errorf("the asset %s does not exist", id)
	}

	return ctx.GetStub().DelState(id)
}

// AssetExists returns true when asset with given ID exists in world state
func (s *SmartContract) TransactionExists(ctx contractapi.TransactionContextInterface, id string) (bool, error) {
	assetJSON, err := ctx.GetStub().GetState(id)
	if err != nil {
		return false, fmt.Errorf("failed to read from world state: %v", err)
	}

	return assetJSON != nil, nil
}

// GetAllAssets returns all assets found in world state
func (s *SmartContract) GetAllTransaction(ctx contractapi.TransactionContextInterface) ([]*Asset, error) {
	// range query with empty string for startKey and endKey does an
	// open-ended query of all assets in the chaincode namespace.
	resultsIterator, err := ctx.GetStub().GetStateByRange("", "")
	if err != nil {
		return nil, err
	}
	defer resultsIterator.Close()

	var assets []*Asset
	for resultsIterator.HasNext() {
		queryResponse, err := resultsIterator.Next()
		if err != nil {
			return nil, err
		}

		var asset Asset
		err = json.Unmarshal(queryResponse.Value, &asset)
		if err != nil {
			return nil, err
		}
		assets = append(assets, &asset)
	}

	return assets, nil
}

func ReadInputFile(inputFile string) ([]int, []string) {

	f, err := os.Open(inputFile)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()
	scanner := bufio.NewScanner(f)
	var energia []int
	var orario []string
	for scanner.Scan() {
		line := scanner.Text()
		splitted_line := strings.Split(line, ",")
		energiaInt, _ := strconv.Atoi(strings.Replace(strings.ToLower(splitted_line[0]), "kw", "", -1))
		energia = append(energia, energiaInt)
		orario = append(orario, strings.Trim(splitted_line[1], " "))
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	return energia, orario
}

func ConvertTimeToDate(time1 string) string {
	currentTime := time.Now()
	splittedTime := strings.Split(time1, ":")
	hour, _ := strconv.Atoi(splittedTime[0])
	minute, _ := strconv.Atoi(splittedTime[1])
	newTime := time.Date(currentTime.Year(), currentTime.Month(), currentTime.Day(), hour, minute, 0, 0, currentTime.Location())
	return newTime.Format("2006-01-02 15:04:05")
}
